 
unsigned int left=0;
unsigned int right=0;

void left_isr()
{
	left++;
}

void right_isr()
{
  right++;
}

void setup()
{
	Serial.begin(115200);
	attachInterrupt(digitalPinToInterrupt(2), left_isr, RISING); 
  attachInterrupt(digitalPinToInterrupt(3), right_isr, RISING); 
}

void loop()
{
	delay(1000);
  Serial.println(left,DEC);
  left=0;
  right=0;

}
