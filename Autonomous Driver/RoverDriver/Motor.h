/*  Simple AVR driving Sketch 
 *   code by: David Faitelson
 *   Afeka Tel-Aviv Academic College of Engineering 
 
 License: CC-SA 3.0, feel free to use this code however you'd like.
 Please improve upon it! Let me know how you've made it better.
 
 */

#pragma once

#include <Arduino.h>

enum Direction { BrakeVCC = 0, Clockwise = 1, CounterClockwise = 2, BrakeGnd = 3 };

class Motor {
public:
  Motor(int inApin, int inBpin, int pwmpin, int icspin);
  void stop();
  void go(Direction direction, int power);
  int currentDraw();
private:
  int inApin; // clockwise input
  int inBpin; // counter-clockwise input
  int pwmpin; // PWM input (motor power)
  int icsPin; // current sense pin 
};
