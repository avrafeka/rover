/*  Simple AVR driving Sketch 
 *   code by: David Faitelson
 *   Afeka Tel-Aviv Academic College of Engineering 
 
 License: CC-SA 3.0, feel free to use this code however you'd like.
 Please improve upon it! Let me know how you've made it better.
 
 */

#include "motor.h"

Motor::Motor(int aPin, int bPin, int pwPin, int icsPin) : inApin(aPin), inBpin(bPin), pwmpin(pwPin), icsPin(icsPin) 
{
  pinMode(inApin, OUTPUT);
  pinMode(inBpin, OUTPUT);
  pinMode(pwmpin, OUTPUT);
  
  digitalWrite(inApin, LOW);
  digitalWrite(inBpin, LOW);
}

int Motor::currentDraw() {
  return analogRead(icsPin);
}

void Motor::stop() 
{
  digitalWrite(inApin, LOW);
  digitalWrite(inBpin, LOW);
}

void Motor::go(Direction direct, int power) 
{
      // Set inA
      if (direct == BrakeVCC || direct == Clockwise) 
      {
        digitalWrite(inApin, HIGH);
      }
      else
      {
        digitalWrite(inApin, LOW);
      }

      // Set inB
      if ( direct== BrakeVCC || direct == CounterClockwise )
      {
        digitalWrite(inBpin, HIGH);
      }
      else
      {
        digitalWrite(inBpin, LOW);
      }
      analogWrite(pwmpin, power);
 }
