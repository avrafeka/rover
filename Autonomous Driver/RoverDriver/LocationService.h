#pragma once

#include <TinyGPS++.h>
#include <SoftwareSerial.h>

class LocationService {
  private:
    TinyGPSPlus gps;

  public:
    LocationService();
    void init();
    void printLocation();
    void delay(unsigned long);
};
