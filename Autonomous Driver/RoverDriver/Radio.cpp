#include <Printers.h>
#include <XBee.h>


#include "radio.h"

Radio::Radio(Stream& s) 
{ 
  xbee.setSerial(s); 
}

boolean Radio::isDataReady(void) {
  xbee.readPacket();
  boolean isAvailable = xbee.getResponse().isAvailable();
  int responseType = xbee.getResponse().getApiId();

  return isAvailable && (responseType == RX_16_RESPONSE || responseType == RX_64_RESPONSE);  
}

int Radio::receive(uint8_t* option, uint8_t* data, int* errorCode) 
{
  if (xbee.getResponse().isError()) {
      *errorCode = xbee.getResponse().getErrorCode();
      return RADIO_INTERNAL_ERROR;
  } 
  
  if (xbee.getResponse().getApiId() == RX_16_RESPONSE) 
  {
      Rx16Response rx16;
      xbee.getResponse().getRx16Response(rx16);
      *option = rx16.getOption();
      memcpy(data, rx16.getData(), rx16.getDataLength());
      return rx16.getDataLength();
  } 
  else if (xbee.getResponse().getApiId() == RX_64_RESPONSE) 
  {
      Rx64Response rx64;
      xbee.getResponse().getRx64Response(rx64);
      *option = rx64.getOption();
      memcpy(data, rx64.getData(), rx64.getDataLength());
      return rx64.getDataLength();
  }
}

void Radio::send(uint16_t addr16, uint8_t *payload, uint8_t payloadLength)
{
  Tx16Request tx = Tx16Request(addr16, payload, payloadLength);
  
  xbee.sendUnescaped(tx);
}
