
/*

 The rover rdriver is a state machine with two states:

  manual      the rover is under manual control, receiving commands from radio
  autonomous  the rover is under autonomous control, receiving commands from serial

 The rover starts in autonomous mode but whenever a radio message arrives it 
 switches to manual mode. It will remain in manual mode until it will receive an
 autonomous mode command. 

 when in manual mode the rover consumes but ignores commands from the autonomous
 controller. 
 
 */
 
#include "Motor.h"

#include "LocationService.h"

#include "Radio.h"

#define AUTONOMOUS 2
#define MANUAL 1

int mode = AUTONOMOUS;

#undef constrain

float constrain(float x, float min, float max) {
  if (x < min) return min;
  if (x > max) return max;
  return x;
}

float mapf(float x, float in_min, float in_max, float out_min, float out_max)
{
  return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
}

Motor left_motor(7,8,5, 2);
Motor right_motor(4,9,6, 3);

float power_left = 0.0;
float power_right = 0.0;

const float low_power = 0; // 13.0;
const float full_power = 64.0;

unsigned long last_comm_time;

const int comm_timeout = 1000; // 1 sec.

bool active;

LocationService locationService;

Radio radio(Serial1);

void setup() {
  
  Serial.begin(115200); // autonomous controller 

  Serial1.begin(115200); // for radio

  locationService.init();
  
  last_comm_time = millis();
  active = false;
}

void loop() {
  
  if (Serial.available()) {

    float left, right;

    left = Serial.parseFloat();
    right = Serial.parseFloat();

    if (mode == AUTONOMOUS)
      do_power_command(left, right);
  }

  if (radio.isDataReady()) {
    
    char data[64] = { 0 };
    uint8_t option = 0;
    int errorCode = 0; 
    
    int len = radio.receive(&option, data, &errorCode);

    if (len > 0) 
    {
      int left, right;
      char code = 0;

      mode = MANUAL;
      
      sscanf(data, "%c %d %d", &code, &left, &right);

      //Serial.println(data);
      switch(code) {
        case 'P':
          do_power_command(left/1023.0, right/1023.0);
          break;
        case 'M':
          mode = AUTONOMOUS;
          break;  
      }
    }
  }
  
  if (active && millis() > last_comm_time + comm_timeout) {

     power_left = 0.0;
     power_right = 0.0;

     left_motor.stop();
     right_motor.stop();

     active = false;
  }

  if (mode == AUTONOMOUS) {
 
    if (Serial.availableForWrite()) {
      locationService.printLocation();
      Serial.println("");
    }
  }
  locationService.delay(20);

}

void do_power_command(float left, float right) {

    //Serial.print(left); Serial.print(" "); Serial.print(right); Serial.println();
  
    power_left = constrain(left, -1.0, 1.0);
    
    power_right = constrain(right, -1.0, 1.0);

    if (power_left >= 0.0) {
      left_motor.go(Clockwise, (uint8_t)mapf(power_left, 0, 1.0, low_power, full_power));
    } else {
      left_motor.go(CounterClockwise, (uint8_t)mapf(-power_left, 0, 1.0, low_power, full_power));      
    }

    if (power_right >= 0.0) {
      right_motor.go(Clockwise, (uint8_t)mapf(power_right, 0, 1.0, low_power, full_power));
    } else {
      right_motor.go(CounterClockwise, (uint8_t)mapf(-power_right, 0, 1.0, low_power, full_power));
    }

    active = true;

    last_comm_time = millis();
}
