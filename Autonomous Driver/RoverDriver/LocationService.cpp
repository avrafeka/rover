#include "LocationService.h"

static const uint32_t GPSBaud = 57600;

LocationService::LocationService()  
{
}

void LocationService::init() {
    Serial2.begin(GPSBaud);
}

void LocationService::printLocation()
{

  if (gps.location.isValid()) {
    Serial.print(gps.location.lat(),6);
    Serial.print(' ');
    Serial.print(gps.location.lng(),6);
  }
}

// This custom version of delay() ensures that the gps object
// is being "fed".
void LocationService::delay(unsigned long ms)
{
  unsigned long start = millis();
  do 
  {
    while (Serial2.available())
      gps.encode(Serial2.read());
  } while (millis() - start < ms);
}
