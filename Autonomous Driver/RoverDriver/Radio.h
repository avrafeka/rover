#ifndef RADIO_H
#define RADIO_H

#include <Arduino.h>
#include <XBee.h>

#define RADIO_INTERNAL_ERROR  -1
#define RADIO_TIMEOUT         -2

class Radio {
  public:
    
    Radio(Stream&);

    boolean isDataReady(void);

    // reads a package from the remote radio into data and option, returns the length (non negative integer) or one of the failure codes (negative integer)
    
    int receive(uint8_t* option, uint8_t* data, int* errorCode);

    void send(uint16_t addr16, uint8_t *payload, uint8_t payloadLength);

  private:
    XBee xbee;
};

#endif 
