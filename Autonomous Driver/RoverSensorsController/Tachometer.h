#pragma once

class Tachometer {

private:

  static unsigned int left;
  static unsigned int right;

  static void left_isr(void);
  static void right_isr(void);
  
public:

  void init();

  void print();
  void reset();
};
