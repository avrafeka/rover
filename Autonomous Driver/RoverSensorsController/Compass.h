#pragma once

#include <Wire.h>
#include <SPI.h>
#include <SparkFunLSM9DS1.h>

class Compass {
  private:

    LSM9DS1 imu;

    float heading;
    float pitch;
    float roll;

    void calcAttitude(float ax, float ay, float az, float mx, float my, float mz);

  public:
    void init();
    bool start();
    void update();
    void print();
};
