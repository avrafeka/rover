#include "LIDAR.h"
#include "Compass.h"
#include "Tachometer.h"

LIDAR lidar;
Compass compass;
Tachometer tachometer;

void setup() {

  Serial.begin(115200);

  tachometer.init();
  compass.init();
  compass.start();
  lidar.init();
  lidar.beginScanning();
}

void loop() {


  if (lidar.period()) {
    Serial.print("H ");
    tachometer.print();
    tachometer.reset();
    Serial.println(" ");
  }

  lidar.scan();
  compass.update();
  
  Serial.print("S ");
  lidar.print();
  Serial.print(' ');
  compass.print();
  Serial.println(" ");  

}
