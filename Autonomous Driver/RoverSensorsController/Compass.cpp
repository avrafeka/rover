// see https://github.com/sparkfun/LSM9DS1_Breakout
#include "Compass.h"

///////////////////////
// Example I2C Setup //
///////////////////////
// SDO_XM and SDO_G are both pulled high, so our addresses are:
#define LSM9DS1_M 0x1E // Would be 0x1C if SDO_M is LOW
#define LSM9DS1_AG  0x6B // Would be 0x6A if SDO_AG is LOW

// Earth's magnetic field varies by location. Add or subtract 
// a declination to get a more accurate heading. Calculate 
// your's here:
// http://www.ngdc.noaa.gov/geomag-web/#declination

#define DECLINATION 4.78 // Declination (degrees) in Tel Aviv, IL

void Compass::init() 
{
  // Before initializing the IMU, there are a few settings
  // we may need to adjust. Use the settings struct to set
  // the device's communication mode and addresses:
  imu.settings.device.commInterface = IMU_MODE_I2C;
  imu.settings.device.mAddress = LSM9DS1_M;
  imu.settings.device.agAddress = LSM9DS1_AG;
  // The above lines will only take effect AFTER calling
  // imu.begin(), which verifies communication with the IMU
  // and turns it on.
}

bool Compass::start() {
  return imu.begin();
}

void Compass::update()
{
  // Update the sensor values whenever new data is available
  if ( imu.gyroAvailable() )
  {
    // To read from the gyroscope,  first call the
    // readGyro() function. When it exits, it'll update the
    // gx, gy, and gz variables with the most current data.
    imu.readGyro();
  }
  if ( imu.accelAvailable() )
  {
    // To read from the accelerometer, first call the
    // readAccel() function. When it exits, it'll update the
    // ax, ay, and az variables with the most current data.
    imu.readAccel();
  }
  if ( imu.magAvailable() )
  {
    // To read from the magnetometer, first call the
    // readMag() function. When it exits, it'll update the
    // mx, my, and mz variables with the most current data.
    imu.readMag();
  }

  calcAttitude(imu.ax, imu.ay, imu.az, -imu.mx, -imu.my, imu.mz);

}

// Calculate pitch, roll, and heading.
// Pitch/roll calculations take from this app note:
// http://cache.freescale.com/files/sensors/doc/app_note/AN3461.pdf?fpsp=1
// Heading calculations taken from this app note:
// http://www51.honeywell.com/aero/common/documents/myaerospacecatalog-documents/Defense_Brochures-documents/Magnetic__Literature_Application_notes-documents/AN203_Compass_Heading_Using_Magnetometers.pdf
void Compass::calcAttitude(float ax, float ay, float az, float mx, float my, float mz)
{
  roll = atan2(ay, az);
  pitch = atan2(-ax, sqrt(ay * ay + az * az));

  // correct the uncalibrated readings 

  mx -= 1082;
  my -= 1260.5;

  // atan2 returns a value between -PI and PI, we would like a value between 0 and 2PI.
  //heading = PI + atan2(mx*cos(roll)+mz*sin(roll), my*cos(pitch)+mx*sin(roll)*sin(pitch)- mz*cos(roll)*sin(pitch));
  heading = PI + atan2(mx, my);
  
  // Convert everything from radians to degrees:
  heading *= 180.0 / PI;
  pitch *= 180.0 / PI;
  roll  *= 180.0 / PI;

  // correct for local declination (while ensuring that heading remains non negative).
  heading = heading + 360 - DECLINATION;

  // readjust again for the range [0, 360)

  heading = fmod(heading, 360);

    // mirror the heading to account for the position of the sensor on the rover
  heading = 360 - heading;

}

void Compass::print() {
  Serial.print(heading);
}
