#pragma once

#include <TinyGPS++.h>
#include <SoftwareSerial.h>

class LocationService {
  private:
    TinyGPSPlus gps;

    // The serial connection to the GPS device
    SoftwareSerial ss; 

  public:
    LocationService();
    void init();
    void printLocation();
    void delay(unsigned long);
};
