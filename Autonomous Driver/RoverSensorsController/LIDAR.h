#pragma once

#include <Servo.h>

class LIDAR {
  
  private:

  Servo horizServo;  

  int vertPos;

  Servo vertServo;  

  int horizPos = 0;    

  int scanAngle = 50;
  int scanDirection = 1;

  int scanDelay = 10;

  int isScanning = false;
  int sendOnlyDistance = false;

  unsigned long distance;
  unsigned long now;

  bool period_;
  
public:

  init();
  
  void beginScanning();

  void endScanning();

  void test();

  void step();
  
  void scan();

  void print(); 
  
  bool period() { return period_; }
};
