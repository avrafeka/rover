#include <Arduino.h>
#include "LIDAR.h"

void LIDAR::beginScanning() {
  horizServo.write(90 - scanAngle/2);
  isScanning = true;
}

void LIDAR::endScanning() {
  isScanning = false;
}

void LIDAR::step() {

  period_ = false;

  if (millis() <= now + 500) {

    distance = pulseIn(4, HIGH, 5000) / 10; // Every 10 usec is equiv to 1cm. Don't wait more than 5 msec (equiv to distance greater than 5 meters)
    if (distance == 0)
      distance = 500;

    // estimate the horizontal angle 
    
    if (scanDirection == 1)
      horizPos = ((millis() - now) / 500.0) * (- scanAngle) + (90 + scanAngle / 2);
    else if (scanDirection == -1)
      horizPos = ((millis() - now) / 500.0) * ( scanAngle) + (90 - scanAngle / 2);

  } else {
    if (scanDirection == 1) {
      horizServo.write(90 - scanAngle / 2);
      scanDirection = -1;
    } else if (scanDirection == -1) {
      horizServo.write(90 + scanAngle / 2);
      scanDirection = 1;
    }
    period_ = true;
    now = millis();    
  }
}

void LIDAR::test() {
/*
  long now;
 
  while(true) {
     horizServo.write(90 + scanAngle / 2);
     now = millis();
     while (millis() < now + 500) {
        pulseWidth = pulseIn(4, HIGH, 4000); // don't wait more than 4 msec (equiv to distance greater than 4 meters)
        if (pulseWidth == 0)
          pulseWidth = 4000;
        Serial.println(pulseWidth / 10);
     }

     horizServo.write(90 - scanAngle / 2);
     now = millis();
     while (millis() < now + 500) {
        pulseWidth = pulseIn(4, HIGH, 4000);  
        if (pulseWidth == 0)
          pulseWidth = 4000;
        Serial.println(pulseWidth / 10);
     }
  }
  */
}

void LIDAR::scan() {

  period_ = false;
  
  distance = pulseIn(4, HIGH, 5000)/10;
  if (distance == 0)
    distance = 500;
    
  horizServo.write(horizPos);
  
  if (horizPos == 90 + scanAngle / 2) 
  {
      period_ =  true;
      scanDirection = -1;
  } else if (horizPos == 90 - scanAngle / 2) 
  {
      period_ = true ;
      scanDirection = 1;
  }

  horizPos += scanDirection;

}

LIDAR::init() {

  vertPos = 107; //102

  scanAngle = 110;
  scanDirection = 1;

  horizServo.attach(9);  
  vertServo.attach(10);  
  
  horizPos = 90 - scanAngle/2;

  pinMode(5, OUTPUT);
  digitalWrite(5, LOW);
  
  pinMode(4, INPUT);

  vertServo.write(vertPos);
  horizServo.write(90 - scanAngle/2);

  isScanning = false;
}

void LIDAR::print() {

    if (distance >= 5) {// readings closer than 5cm are noise
      Serial.print(distance); 
      Serial.print(" ");
      Serial.print(horizPos);
    }
}
