#include "LocationService.h"

static const int RXPin = 6, TXPin = 7; // for Arduino Nano 
static const uint32_t GPSBaud = 57600;

LocationService::LocationService() : ss(RXPin, TXPin) 
{
}

void LocationService::init() {
  ss.begin(GPSBaud);
}

void LocationService::printLocation()
{

  Serial.print(gps.location.lat());
  Serial.print(' ');
  Serial.print(gps.location.lng());
}

// This custom version of delay() ensures that the gps object
// is being "fed".
void LocationService::delay(unsigned long ms)
{
  unsigned long start = millis();
  do 
  {
    while (ss.available())
      gps.encode(ss.read());
  } while (millis() - start < ms);
}
