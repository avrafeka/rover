#include <Arduino.h>
#include "Tachometer.h"

const float distancePerTick = 35.6; // mm

unsigned int Tachometer::left=0;

unsigned int Tachometer::right=0;

void Tachometer::left_isr()
{
  left++;
}

void Tachometer::right_isr()
{
  right++;
}

void Tachometer::init()
{
  attachInterrupt(digitalPinToInterrupt(2), left_isr, RISING); 
  attachInterrupt(digitalPinToInterrupt(3), right_isr, RISING); 
}

void Tachometer::reset() 
{
  left=0;
  right=0;
}

void Tachometer::print() {

  noInterrupts();
  unsigned long m = millis();
  unsigned long l = left;
  unsigned long r = right;
  interrupts();
  
  Serial.print(m);
  Serial.print(' ');
  Serial.print(l*distancePerTick);
  Serial.print(' ');
  Serial.print(r*distancePerTick);
}
